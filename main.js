const express = require("express");
const app = express();
const port = 4000;

app.get("/", (req, res) => {
  res.send("<h1>Hello from simple server></h1>");
});

app.listen(port, () =>
  console.log("Server is up and running on port : " + port)
);
